const { Schema, model } = require("mongoose");

const application = new Schema({
  fullname: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
  service: {
    type: String,
  },
  consultation: {
    type: String,
  },
  schooldiabetes: {
    type: String,
  },
  // convenientTime: {
  //     type: String,
  // },
  question: {
    type: String,
  },
  date: String
});

module.exports = model("Application", application);
