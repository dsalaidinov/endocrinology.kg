const {Router} = require('express');
const router = Router();

router.get('/', (req, res) => {
    res.render('protocols', {
        title: 'Клинические протоколы и руководства',
        isServices: true
    });
});

module.exports = router;