const {Router} = require('express');
const path = require('path');
const router = Router();

router.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '../../sitemap.xml'));
});

module.exports = router;