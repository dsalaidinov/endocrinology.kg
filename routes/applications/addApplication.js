const {Router} = require('express');
const Application = require('../../models/application');
const router = Router();

router.post('/', async (req, res) => {
    let {fullname, phone, 
      service, consultation, 
      schooldiabetes, convenientTime, 
      date, question} = req.body;

    if(service === 'consultation-button') service = 'Консультация'; 
    if(service === 'schooldiabetes-button') service = 'Школа диабета'; 

    const application = new Application({
      fullname: fullname,
      phone: phone,
      service: service,
      consultation: consultation,
      schooldiabetes: schooldiabetes,
      convenientTime: convenientTime,
      date: date,
      question: question
    });
    
    try {
        await application.save();
        res.redirect('/');
    } catch (e) {
        console.log(e);
    }
})

module.exports = router;