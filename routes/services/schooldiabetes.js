const {Router} = require('express');
const router = Router();

router.get('/', (req, res) => {
    res.render('schooldiabetes', {
        title: 'Школа диабета',
        isServices: true
    });
});

module.exports = router;