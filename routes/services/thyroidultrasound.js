const {Router} = require('express');
const router = Router();

router.get('/', (req, res) => {
    res.render('thyroidultrasound', {
        title: 'УЗИ щитовидной железы',
        isServices: true
    });
});

module.exports = router;