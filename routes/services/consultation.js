const {Router} = require('express');
const router = Router();

router.get('/', (req, res) => {
    res.render('consultation', {
        title: 'Консультация',
        isServices: true
    });
});

module.exports = router;