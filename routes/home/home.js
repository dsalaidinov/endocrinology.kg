const {Router} = require('express');
const Post = require('../../models/post');
const router = Router();

router.get('/', async (req, res) => {
    const posts = await Post.find().sort({ _id: -1 }).limit(2);
    
    res.render('index', {
        title: '',
        isHome: true,
        posts: posts,
        helpers: {
            sliceText: function (text) { return text.slice(0, 15); },
        }
    })
})

module.exports = router;