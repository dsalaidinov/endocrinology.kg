const {Router} = require('express');
const router = Router();

router.get('/', (req, res) => {
    res.render('about', {
        title: 'Обо мне',
        isHome: true
    })
})

module.exports = router;