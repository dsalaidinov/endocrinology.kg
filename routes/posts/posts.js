const {Router} = require('express');
const Post = require('../../models/post');
const router = Router();

router.get('/', async (req, res) => {
    const posts = await Post.find();
    res.render('posts', {
        title: 'Статьи',
        isPosts: true,
        posts: posts,
        helpers: {
            // sliceText: function (text) { return text.slice(0, 15); },
            eachRow: function (items, numColumns, options) { 
                let result = '';
                for (var i = 0; i < items.length; i += numColumns) {
                    result += options.fn({
                        columns: items.slice(i, i + numColumns)
                    });
                }

                return result;
            },
        }
    })
})


router.get('/:id/edit', async (req, res) => {
    if(!req.query.allow) {
        return res.redirect('/')
    }
})

router.get('/:id', async (req, res) => {
    let post = '';
    try {
        post = await Post.findById(req.params.id)
    } catch (e) {
        console.log(e);
        
    }    
    res.render('post', {
        // layout: 'single',
        title: post.title, 
        post
    });
})


module.exports = router;