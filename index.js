const express = require("express");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoStore = require("connect-mongodb-session")(session);
const exphbs = require("express-handlebars");
const csurf = require("csurf");
const helmet = require("helmet");
const compression = require("compression");
const flash = require("connect-flash");
const Handlebars = require("handlebars");
const {
  allowInsecurePrototypeAccess,
} = require("@handlebars/allow-prototype-access");
const path = require("path");
const homeRouter = require("./routes/home/home");
const aboutRouter = require("./routes/about/about");
const servicesRouter = require("./routes/services/services");
const treatmentRouter = require("./routes/services/treatment");
const consultationRouter = require("./routes/services/consultation");
const schooldiabetesRouter = require("./routes/services/schooldiabetes");
const thyroidultrasoundRouter = require("./routes/services/thyroidultrasound");
const biopsyRouter = require("./routes/services/biopsy");
const pompaRouter = require("./routes/services/pompa");
const monitoringRouter = require("./routes/services/monitoring");
const protocolsRouter = require("./routes/fordoctors/protocols");
const postsRouter = require("./routes/posts/posts");
const adminRouter = require("./routes/admin/admin");
const addApplicationRouter = require("./routes/applications/addApplication");
const authRouter = require("./routes/auth/auth");
const sitemapRouter = require("./routes/sitemap/sitemap");
const varMiddleware = require("./middleware/variables");
const userMiddleware = require("./middleware/user");
const errorHandler = require("./middleware/error");
const fileMiddleware = require("./middleware/file");
const keys = require("./keys");
const PORT = process.env.PORT || 3000;
const app = express(),
  os = require("os"),
  cluster = require("cluster");

const hbs = exphbs.create({
  defaultLayout: "main",
  extname: "hbs",
  handlebars: allowInsecurePrototypeAccess(Handlebars),
});

app.engine("hbs", hbs.engine);
app.set("view engine", "hbs");
app.set("views", "views");
const store = new MongoStore({
  collection: "sessions",
  uri: keys.MONGODB_URI,
});

Handlebars.registerHelper("sliceText", function (text) {
  return text.slice(0, 1);
});
app.use(express.static(path.join(__dirname, "public")));
app.use("/images", express.static(path.join(__dirname, "images")));
app.use(express.urlencoded({ extended: false }));
app.use(
  session({
    secret: keys.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    store,
  })
);
app.use(fileMiddleware.single("img"));
app.use(csurf());
app.use(flash());
app.use(helmet());
app.use(varMiddleware);
app.use(userMiddleware);

app.use("/", homeRouter);
app.use("/about", aboutRouter);
app.use("/services", servicesRouter);
app.use("/consultation", consultationRouter);
app.use("/treatment", treatmentRouter);
app.use("/schooldiabetes", schooldiabetesRouter);
app.use("/thyroidultrasound", thyroidultrasoundRouter);
app.use("/biopsy", biopsyRouter);
app.use("/pompa", pompaRouter);
app.use("/monitoring", monitoringRouter);

app.use("/protocols", protocolsRouter);

app.use("/posts", postsRouter);
app.use("/admin", adminRouter);
app.use("/addapplication", addApplicationRouter);
app.use("/auth", authRouter);
app.use("/sitemap.xml", sitemapRouter);

app.use(errorHandler);
app.use((req, res, next) => {
  if (cluster.isWorker)
    console.log(`Worker ${cluster.worker.id} handle request`);

  next();
});

async function start() {
  try {
    await mongoose.connect(keys.MONGODB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });

    if (cluster.isMaster) {
      let cpus = os.cpus().length;

      for (let i = 0; i < cpus; i++) cluster.fork();

      cluster.on("exit", (worker, code) => {
        console.log(`Worker ${worker.id} finished. Exit code: ${code}`);

        app.listen(PORT, () => {
          console.log(`server is running on port ${PORT}`);
        });
      });
    } else {
      app.listen(PORT, () => {
        console.log(`server is running on port ${PORT}`);
      });
    }
  } catch (e) {
    console.log(e);
  }
}

start();

module.exports = app;
